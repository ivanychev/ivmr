# Computational Graph

The library contains the implementation of a prototype of a computational graph.
A CG is a set of basic functional operations (map, reduce, join, etc.)
with connected inputs or outputs which can be assembled in 
complex pipelines an used in solving complex data processing
problems.

## Usage

Typical example of usage:

```python
import ivmr


def square(rec):
    yield {"value": rec["first"] * rec["first"]}


g = ivmr.ComputeGraph("input", "output")
g.add_operation(ivmr.Map(square),
                {"input_table": ("GRAPH", "input")},
                op_id="square")
g.set_input("input", some_data)
res = g.get_output("output")
```

Each operation has it's own set of input and output tags
(which are similar to ip:port pair). Graph creation consists
of adding operation and piping outputs of one to an input of another.
See more examples in unit tests.

## Project structure

* `/ivmr` — is the library itself.
    * `/ivmr/operations` — operations which are used to assemble graph.
    * `/ivmr/graph` — implementation of the compute graph
    * `/ivmr/misc` — some utilities used in the implementation.
* `/examples` — some problems solved using the compute graph.

Unit tests (if needed) are included in corresponding folders. They
can be launched via

```bash
python3 -m pytest
```

In `/ivmr` directory.

## Dependencies

* Python 3.6
* [pytest](https://docs.pytest.org/en/latest/)
* [toolz](https://toolz.readthedocs.io/en/latest/)

## Author

Sergey Ivanychev

sergeyivanychev@gmail.com