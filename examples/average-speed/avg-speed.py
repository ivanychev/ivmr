import datetime
import math
import sys

from typing import (
    List,
    Tuple
)

import ivmr


SECS_IN_HOUR = 60 * 60
EARTH_RADIUS = 6373.0
METERS_IN_KM = 1000


def distance(record):
    lon1, lat1 = record["start"]
    lon2, lat2 = record["end"]

    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat1)
    lon2 = math.radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = (math.sin(dlat / 2) ** 2 +
         math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return EARTH_RADIUS * c


def to_datetime(raw_datetime: str) -> datetime.datetime:
    # Like that: 20170912T123410.1794

    parts = raw_datetime.split(".")
    without_milli = parts[0]
    dt = datetime.datetime.strptime(without_milli, "%Y%m%dT%H%M%S")
    if len(parts) > 1:
        dt += datetime.timedelta(milliseconds=float(parts[-1]))
    return dt


def cut_by_hour(datetime_object: datetime.datetime) -> datetime.datetime:
    return datetime.datetime(year=datetime_object.year,
                             month=datetime_object.month,
                             day=datetime_object.day,
                             hour=datetime_object.hour)


def get_segments(
        start: datetime.datetime,
        end: datetime.datetime
) -> List[Tuple[datetime.datetime, datetime.datetime]]:
    hour = datetime.timedelta(hours=1)
    points = [start]
    next_hour_point = cut_by_hour(start) + hour
    while next_hour_point < end:
        points.append(next_hour_point)
        next_hour_point += hour
    points.append(end)
    return [(points[i], points[i + 1]) for i in range(len(points) - 1)]


def to_distance_and_time(record):
    enter_time = to_datetime(record['enter_time'])
    leave_time = to_datetime(record['leave_time'])
    total_time = leave_time - enter_time
    if not total_time:
        return
    hour_segments = get_segments(enter_time, leave_time)
    for segment_start, segment_end in hour_segments:
        time_ratio = (segment_end - segment_start) / total_time
        yield {
            "weekday": segment_start.weekday(),
            "hour": segment_start.hour,
            "distance": distance(record) * time_ratio,
            "time_spent": segment_end - segment_start
        }


def avg_speed_extract(records):
    records = list(records)
    all_distance = sum(r["distance"] for r in records)
    all_time = sum(abs(r["time_spent"].total_seconds()) for r in records)

    km_per_hour = (all_distance / METERS_IN_KM) / (all_time / SECS_IN_HOUR)
    yield {
        "weekday": records[0]["weekday"],
        "hour": records[0]["hour"],
        "avg_speed_kmh": km_per_hour
    }


def main():
    travel_times = ivmr.records_from_file("../data/travel_times.txt")
    graph_data = ivmr.records_from_file("../data/graph_data.txt")
    g = ivmr.ComputeGraph(["travel_times", "graph_data"], "output")

    g.add_operation(ivmr.Join("edge_id", "edge_id", ivmr.JoinStrategy.INNER),
                    {"left_table": ("GRAPH", "travel_times"),
                     "right_table": ("GRAPH", "graph_data")},
                    "table_joiner")
    g.add_operation(ivmr.Map(to_distance_and_time),
                    {"input_table": ("table_joiner", "output_table")},
                    "segment_extractor")
    g.add_operation(ivmr.Sort((("weekday", ivmr.SortOrder.ASC),
                               ("hour", ivmr.SortOrder.ASC))),
                    {"input_table": ("segment_extractor", "output_table")},
                    "segment_sort")
    g.add_operation(ivmr.Reduce(("weekday", "hour"), avg_speed_extract),
                    {"input_table": ("segment_sort", "output_table"),
                     "output_table": ("GRAPH", "output")})

    g.set_input("travel_times", travel_times)
    g.set_input("graph_data", graph_data)
    output = g.get_output("output")
    ivmr.records_to_file("avg-speed.txt", output)


if __name__ == "__main__":
    sys.exit(main())
