import itertools
import math
import sys
import re

from typing import Any, Dict, Generator, List

import ivmr

WORD_REGEX = r"\b[a-z]+\b"


def text_to_words(text: str):
    """Splits text and returns generator of words in it."""
    for match in re.finditer(WORD_REGEX, text, flags=re.I):
        word = match.group()
        yield word.lower()


def word_splitter(
        r: Dict[str, str]
) -> Generator[Dict[str, List[str]], None, None]:
    """Yields a pair of doc_id and word for each word in text field."""
    words = text_to_words(r['text'])
    for word in words:
        yield {
            'doc_id': r['doc_id'],
            'word': word
        }


def leave_one(records):
    """Leaves exactly one element from each group."""
    for record in records:
        yield record
        break


def calc_idf(records):
    """Calculates idf.

    :param records: dict with the same "word" field.
    :yields: dict. Counts number of records with this word.
    """
    records = list(records)
    yield {
        "word": records[0]["word"],
        "idf": len(records),
        "docs_number": records[0]["docs_number"]
    }


def docs_counter(
        rec1: Dict[str, int],
        _: Dict[str, Any]
) -> Dict[str, int]:
    """Counts number of records. Should be used as a reducer function."""
    rec1["docs_number"] += 1
    return rec1


def calc_tf(records):
    """Calculates tf.

    :param records: dict with the same "doc_id" and "word" fields.
    :yields: dict. Count number of word occurrences in current doc.
    """
    records = list(records)
    yield {
        "doc_id": records[0]["doc_id"],
        "word": records[0]["word"],
        "tf": len(records)
    }


def tf_idf(r):
    return r["tf"] * math.log(r["docs_number"] / r["idf"])


def cut_top_n(records, top=3):
    """Yields top-n records by tf-idf."""
    records = sorted(list(records), key=tf_idf)
    output = (
        {"doc_id": r["doc_id"],
         "word": r["word"],
         "tf-idf": tf_idf(r)}
        for r in itertools.islice(records, top)
    )
    yield from output


def main():
    input_data = ivmr.records_from_file("../data/text_corpus.txt")
    g = ivmr.ComputeGraph("input", "output")
    g.add_operation(ivmr.Map(word_splitter),
                    {"input_table": ("GRAPH", "input")},
                    "word_splitter")
    # IDF.
    g.add_operation(ivmr.Sort((("doc_id", ivmr.SortOrder.ASC),
                               ("word", ivmr.SortOrder.ASC))),
                    {"input_table": ("word_splitter", "output_table")},
                    "doc_word_sorter")
    g.add_operation(ivmr.Fold(zero={"docs_number": 0},
                              folder=docs_counter),
                    {"input_table": ("GRAPH", "input")},
                    "docs_counter")
    g.add_operation(ivmr.Reduce(("doc_id", "word"), leave_one),
                    {"input_table": ("doc_word_sorter", "output_table")},
                    "one_pair_leaver")
    g.add_operation(ivmr.Join(None, None, ivmr.JoinStrategy.CROSS),
                    {"left_table": ("one_pair_leaver", "output_table"),
                     "right_table": ("docs_counter", "output_table")},
                    "joiner_with_n_docs")
    g.add_operation(ivmr.Sort((("word", ivmr.SortOrder.ASC), )),
                    {"input_table": ("joiner_with_n_docs", "output_table")},
                    "idf_word_sorter")
    g.add_operation(ivmr.Reduce(("word", ), calc_idf),
                    {"input_table": ("idf_word_sorter", "output_table")},
                    "idf_extractor")

    # TF.
    g.add_operation(ivmr.Reduce(("doc_id", "word"), calc_tf),
                    {"input_table": ("doc_word_sorter", "output_table")},
                    "tf_extractor")
    # Calc tf-idf.
    g.add_operation(ivmr.Join("word", "word", ivmr.JoinStrategy.LEFT),
                    {"left_table": ("tf_extractor", "output_table"),
                     "right_table": ("idf_extractor", "output_table")},
                    "tf-idf-joiner")
    g.add_operation(ivmr.Reduce(("word", ), cut_top_n),
                    {"input_table": ("tf-idf-joiner", "output_table"),
                     "output_table": ("GRAPH", "output")},
                    "tf-idf-cutter")

    g.set_input("input", input_data)
    output = g.get_output("output")
    ivmr.records_to_file("tf-idf.txt", output)


if __name__ == "__main__":
    sys.exit(main())
