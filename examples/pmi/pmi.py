import sys
import re

from typing import Dict, Generator, List

import ivmr

WORD_REGEX = r"\b[a-z]+\b"


def text_to_words(text: str):
    """Splits text and returns generator of words in it."""
    for match in re.finditer(WORD_REGEX, text, flags=re.I):
        word = match.group()
        yield word.lower()


def word_splitter(
        r: Dict[str, str]
) -> Generator[Dict[str, List[str]], None, None]:
    """Yields a pair of doc_id and word for each word in text field."""
    words = text_to_words(r['text'])
    for word in words:
        yield {
            'doc_id': r['doc_id'],
            'word': word
        }


def total_word_count(records):
    records = list(records)
    yield {
        "word": records[0]["word"],
        "total_count": len(records)
    }


def doc_word_count(records):
    records = list(records)
    yield {
        "word": records[0]["word"],
        "doc_id": records[0]["doc_id"],
        "in_doc_count": len(records)
    }


def pmi_extractor(record):
    yield {
        "word": record["word"],
        "doc_id": record["doc_id"],
        "pmi": record["in_doc_count"] / record["total_count"]
    }


def main():
    input_data = ivmr.records_from_file("../data/text_corpus.txt")
    g = ivmr.ComputeGraph("input", "output")
    g.add_operation(ivmr.Map(word_splitter),
                    {"input_table": ("GRAPH", "input")},
                    "word_splitter")
    g.add_operation(ivmr.Sort((("word", ivmr.SortOrder.ASC),
                               ("doc_id", ivmr.SortOrder.ASC))),
                    {"input_table": ("word_splitter", "output_table")},
                    "doc_word_sorter")
    g.add_operation(ivmr.Reduce(("word", ), total_word_count),
                    {"input_table": ("doc_word_sorter", "output_table")},
                    "total_word_counter")
    g.add_operation(ivmr.Reduce(("word", "doc_id"), doc_word_count),
                    {"input_table": ("doc_word_sorter", "output_table")},
                    "doc_word_counter")
    g.add_operation(ivmr.Join("word", "word", ivmr.JoinStrategy.LEFT),
                    {"left_table": ("doc_word_counter", "output_table"),
                     "right_table": ("total_word_counter", "output_table")},
                    "freqs_joiner")
    g.add_operation(ivmr.Map(pmi_extractor),
                    {"input_table": ("freqs_joiner", "output_table"),
                     "output_table": ("GRAPH", "output")},
                    "pmi_extractor")

    g.set_input("input", input_data)
    output = g.get_output("output")
    ivmr.records_to_file("pmi.txt", output)


if __name__ == "__main__":
    sys.exit(main())
