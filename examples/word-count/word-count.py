import collections
import sys
import re

from typing import Dict, Generator

import ivmr

WORD_REGEX = r"\b[a-z]+\b"


def text_to_words(text: str):
    for match in re.finditer(WORD_REGEX, text, flags=re.I):
        word = match.group()
        yield word.lower()


def word_counter(
        r: Dict[str, str]
) -> Generator[Dict[str, collections.Counter], None, None]:
    yield {"counts": collections.Counter(text_to_words(r['text']))}


def counts_summer(
        rec1: Dict[str, collections.Counter],
        rec2: Dict[str, collections.Counter]
) -> Dict[str, collections.Counter]:
    rec1["counts"] = rec1["counts"] + rec2["counts"]
    return rec1


def main():
    input_data = ivmr.records_from_file("../data/text_corpus.txt")
    g = ivmr.ComputeGraph("input", "output")
    g.add_operation(ivmr.Map(word_counter),
                    {"input_table": ("GRAPH", "input")},
                    "word_splitter")
    g.add_operation(ivmr.Fold(zero={"counts": collections.Counter()},
                              folder=counts_summer),
                    {"input_table": ("word_splitter", "output_table"),
                     "output_table": ("GRAPH", "output")},
                    "counts_summer")
    g.set_input("input", input_data)
    output = g.get_output("output")
    ivmr.records_to_file("word-count.txt", output)


if __name__ == "__main__":
    sys.exit(main())
