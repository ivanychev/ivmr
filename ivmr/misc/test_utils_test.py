import pytest

from ivmr.misc import test_utils


def test_empty_generator():
    g = test_utils.empty_generator()
    assert not len(list(g))

def test_dummy_callable():
    f = test_utils.dummy_record_callable()
    g = f()
    assert not len(list(g))