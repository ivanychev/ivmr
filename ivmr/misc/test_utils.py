
from ivmr.misc import utils


def empty_generator():
    """Creates an empty generator."""
    return (x for x in tuple())


def dummy_record_callable():
    """Creates a callable that returns an empty generator."""
    return empty_generator


def to_operation_input(dict_iterable):
    record_gen = (record for record in dict_iterable)
    return utils.generator_to_callable(record_gen)