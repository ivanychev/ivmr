import types

import pytest

from ivmr.misc import utils

@pytest.mark.parametrize("iterable", [
    ([1, 2, 3], ),
    ("azazaz", ),
    ([], ),
    (range(1, 6, 2), )
])
def test_iterable_to_generator(iterable):
    gen = utils.iterable_to_generator(iterable)
    assert isinstance(gen, types.GeneratorType)
    assert list(iterable) == list(gen)


def test_generator_to_callable():
    generator = (x for x in range(10))
    gen_callable = utils.generator_to_callable(generator)
    actual_gen = gen_callable()
    assert isinstance(actual_gen, types.GeneratorType)
    assert actual_gen is generator

@pytest.mark.parametrize("values", [
    [1, 2, 3],
    [1],
    ['sergey'],
    [3, 1, 4, 1, 5, 92, 6]
])
def test_peek_from_generator_nonempty(values):
    gen = (v for v in values)
    first, gen = utils.peek_from_generator(gen, object())
    assert first == values[0]
    assert values == list(gen)

def test_peek_from_generator_empty():
    gen = (v for v in range(0))
    sentinel = object()
    first, new_gen = utils.peek_from_generator(gen, sentinel)
    assert first is sentinel
    assert isinstance(new_gen, types.GeneratorType)
    assert not len(list(new_gen))