class OperationIOError(Exception):
    pass


class DataConditionError(Exception):
    pass