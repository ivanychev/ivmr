import itertools
import json

from typing import (
    Any,
    Generator,
    Iterable,
    Tuple,
    Union
)

from ivmr.misc import ptypes


AnyGenerator = Generator[Any, None, None]


def iterable_to_generator(it: Iterable[ptypes.Record]) -> ptypes.RecordGenerator:
    """Creates a generator that yields values from the given iterable object."""
    return (value for value in it)


def generator_to_callable(
        gen: ptypes.RecordGenerator) -> ptypes.RecGeneratorCallable:
    """Returns lambda that returns passed generator."""
    return lambda: gen


def peek_from_generator(gen: AnyGenerator,
                        sentinel: object) -> Tuple[Any,
                                                   Union[itertools.chain,
                                                         AnyGenerator]]:
    """Peeks the first element from the generator and leaves it unchanged.

    :param gen: Generator object. The generator from which we're peeking an
        object.
    :param sentinel: The object that is returned if the generator is empty.
    :returns: The tuple of the first element from the generator and the
        unchanged generator. If the generator was empty, the sentinel is used
        as the first element of the tuple.
    """
    gen_iterator = iter(gen)
    try:
        elem = next(gen_iterator)
    except StopIteration:
        # There's no next element. Return the empty generator with the sentinel.
        return sentinel, gen
    # Returning the element back to the generator so as to make it unchanged.
    recovered_gen = itertools.chain([elem], gen)
    return elem, recovered_gen


def records_from_file(filename: str) -> ptypes.RecordGenerator:
    """Creates record generator via reading file, that contains json objects.
    Each row must contain exactly one JSON.

    :param filename: str. Path to the input file.
    :return: Dict generator.
    """
    with open(filename, "r") as f:
        for row in f:
            yield json.loads(row)


def records_to_file(filename: str, records: ptypes.RecordIterable) -> None:
    """Writes record iterable to a file."""
    with open(filename, "w") as f:
        buffer = (json.dumps(record) for record in records)
        f.write("\n".join(buffer))


def to_iterable(obj: ptypes.IterableOr[ptypes.T], cls: type) -> Iterable[ptypes.T]:
    """Makes sure that the input object is an iterable of type T, not a T."""
    if isinstance(obj, cls):
        return [obj]
    return obj
