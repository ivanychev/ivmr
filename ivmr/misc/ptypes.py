from typing import (
    Any,
    Callable,
    Generator,
    Dict,
    Iterable,
    TypeVar,
    Union
)

Record = Dict[str, Any]
RecordGenerator = Generator[Record, None, None]
RecordIterable = Iterable[Record]
RecGeneratorCallable = Callable[[], RecordGenerator]
RecIterableCallable = Callable[[], Iterable[Record]]
GeneratorFactory = Callable[[Record], RecordGenerator]

T = TypeVar('T')
IterableOr = Union[Iterable[T], T]