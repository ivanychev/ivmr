import pytest

from ivmr.misc import exceptions
from ivmr.misc import utils
from ivmr.misc import test_utils
from ivmr.operations import reduce


@pytest.fixture
def unsorted_fake_data():
    raw_data = [
        (2, 3),
        (1, 1),
        (1, 3),
        (1, 2),
        (2, 1),
        (2, 2)
    ]
    data = [{"first": x[0], "second": x[1]} for x in raw_data]
    record_gen = (record for record in data)
    return {"input_table": utils.generator_to_callable(record_gen)}


@pytest.fixture
def fake_data():
    raw_data = [
        (1, 3),
        (1, 1),
        (1, 3),
        (1, 2),
        (2, 1),
        (2, 2)
    ]
    data = [{"first": x[0], "second": x[1]} for x in raw_data]
    record_gen = (record for record in data)
    return {"input_table": utils.generator_to_callable(record_gen)}


@pytest.mark.parametrize("raw_records, key, expected", [
    ([(1, 2), (2, 2), (2, 2), (3, 2)], "0", False),
    ([(3, 2), (2, 2), (2, 2), (1, 2)], "0", True),
    ([(1, 2), (2, 2), (2, 2), (3, 2), (1, 2)], "0", True),
    ([(3, 2), (2, 2), (2, 2), (3, 2), (4, 2)], "0", True),
    ([(1, 2)], "0", False),
    ([], "0", False)
])
def test_is_not_sorted(raw_records, key, expected):
    records = [
        {str(key): val for key, val in enumerate(raw_record)}
        for raw_record in raw_records
    ]
    assert expected == reduce._is_not_sorted(records, (key, ))


@pytest.mark.parametrize("raw_data, expected", [
    ([
        {"1": 1, "2": 1},
        {"1": 1, "2": 2},
        {"1": 2, "2": 1},
        {"1": 2, "2": 2},
    ], False),
    ([
        {"1": 1, "2": 2},
        {"1": 1, "2": 1},
        {"1": 2, "2": 1},
        {"1": 2, "2": 2},
    ], True),
    ([
        {"1": 1, "2": 1},
        {"1": 2, "2": 2},
        {"1": 2, "2": 1},
        {"1": 2, "2": 2},
    ], True),
])
def test_is_not_sorted_complex_key(raw_data, expected):
    assert expected == reduce._is_not_sorted(raw_data, ("1", "2"))


def dummy_factory(records):
    yield


def test_reduce_not_sorted(unsorted_fake_data):
    op = reduce.Reduce("first", dummy_factory, inputs=unsorted_fake_data)
    with pytest.raises(exceptions.DataConditionError):
        _ = op.get_output("output_table")


def summer(records):
    records = list(records)
    rec = {"first": records[0]["first"],
           "second": sum(record["second"] for record in records)}
    yield rec


def test_reduce_summer(fake_data):
    op = reduce.Reduce("first", summer, inputs=fake_data)
    expected = [
        {"first": 1, "second": 9},
        {"first": 2, "second": 3}
    ]
    output = op.get_output("output_table")
    result = list(output)
    assert expected == result


def sum_keys(records):
    yield {"sum": sum(sum(r.values()) for r in records)}


def test_reduce_complex_key():
    data = [
        {"1": 1, "2": 1},
        {"1": 1, "2": 1},
        {"1": 1, "2": 2},
        {"1": 2, "2": 2},
        {"1": 2, "2": 2}
    ]
    input = test_utils.to_operation_input(data)
    op = reduce.Reduce(("1", "2"), sum_keys, inputs={"input_table": input})
    actual_iterable = op.get_output("output_table")
    expected = [
        {"sum": 4},
        {"sum": 3},
        {"sum": 8}
    ]
    assert expected == list(actual_iterable)


def test_reduce_summer_cached(fake_data):
    op = reduce.Reduce("first", summer, inputs=fake_data, cached=True)
    expected = [
        {"first": 1, "second": 9},
        {"first": 2, "second": 3}
    ]
    output = op.get_output("output_table")
    result = list(output)
    assert expected == result

    output = op.get_output("output_table")
    result = list(output)
    assert expected == result


def one_yielder(records):
    records = list(records)
    for _ in range(sum(record["second"] for record in records)):
        yield {"first": records[0]["first"], "second": "foo"}


def test_reduce_one_yielder(fake_data):
    op = reduce.Reduce("first", one_yielder, inputs=fake_data)
    expected = (([{"first": 1, "second": "foo"}] * 9) +
                ([{"first": 2, "second": "foo"}] * 3))
    output = op.get_output("output_table")
    result = list(output)
    assert expected == result
