import pytest

from ivmr.misc import utils
from ivmr.operations import fold


@pytest.fixture
def fake_data():
    raw_data = [
        (2, 3),
        (1, 1),
        (1, 3),
        (1, 2),
        (2, 1),
        (2, 2)
    ]
    data = [{"first": x[0], "second": x[1]} for x in raw_data]
    record_gen = (record for record in data)
    return {"input_table": utils.generator_to_callable(record_gen)}


def summer(rec1, rec2):
    return {key: rec1[key] + rec2[key] for key in rec1}


def first_summer(rec1, rec2):
    return {"first": rec1["first"] + rec2["first"], "second": rec1["second"]}


@pytest.mark.parametrize("folder, zero, expected", [
    (summer, {"first": 0, "second": 0}, {"first": 9, "second": 12}),
    (first_summer, {"first": 0, "second": -100}, {"first": 9, "second": -100})
])
def test_fold(fake_data, folder, zero, expected):
    op = fold.Fold(zero, folder, inputs=fake_data)
    one_value_list = list(op.get_output("output_table"))
    assert len(one_value_list) == 1
    actual = one_value_list[0]
    assert expected == actual


@pytest.mark.parametrize("folder, zero, expected", [
    (summer, {"first": 0, "second": 0}, {"first": 9, "second": 12}),
    (first_summer, {"first": 0, "second": -100}, {"first": 9, "second": -100})
])
def test_fold_cached(fake_data, folder, zero, expected):
    op = fold.Fold(zero, folder, inputs=fake_data, cached=True)
    one_value_list = list(op.get_output("output_table"))
    assert len(one_value_list) == 1
    actual = one_value_list[0]
    assert expected == actual

    one_value_list = list(op.get_output("output_table"))
    assert len(one_value_list) == 1
    actual = one_value_list[0]
    assert expected == actual
