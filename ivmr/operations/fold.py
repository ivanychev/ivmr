import functools
from typing import Callable, Dict, Optional, Tuple

from ivmr.misc import exceptions
from ivmr.misc import ptypes
from ivmr.misc import utils
from ivmr.operations import base

FolderFunc = Callable[[ptypes.Record, ptypes.Record], ptypes.Record]


class Fold(base.BaseOperation):
    INPUT_TAGS: Tuple[str, ...] = ("input_table",)
    OUTPUT_TAGS: Tuple[str, ...] = ("output_table",)

    def __init__(self,
                 zero: ptypes.Record,
                 folder: FolderFunc,
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None,
                 cached: bool=False):
        """Fold operation that reduces the table to the single row.

        :param zero: The initial element.
        :param folder: The reducer callable.
        :param inputs: The mapping from input tags to record iterables.
        :param cached: bool. The parameter denotes if the operation should
            operate in cached mode.
        """
        base.BaseOperation.__init__(self, inputs, zero, folder, cached=cached)
        self._zero = zero
        self._cached = cached
        self._cached_output = None
        self._folder = folder

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        """The implementation of the computation."""
        if tag != "output_table":
            raise exceptions.OperationIOError(
                "Fold received an invalid output tag")
        if self._cached and self._cached_output is not None:
            return utils.iterable_to_generator(self._cached_output)

        folded = functools.reduce(self._folder,
                                  self._inputs["input_table"](),
                                  self._zero)
        tuple_with_value = (folded, )
        if self._cached:
            self._cached_output = tuple_with_value
        return utils.iterable_to_generator(tuple_with_value)
