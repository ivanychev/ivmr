import pytest

from ivmr.misc import exceptions
from ivmr.misc import test_utils
from ivmr.operations import base


class DummyOperation(base.BaseOperation):
    INPUT_TAGS = ("it1", "it2")
    OUTPUT_TAGS = ("ot1", "ot2", "ot3")


@pytest.fixture
def dummy_operation():
    return DummyOperation({"it1": test_utils.dummy_record_callable,
                           "it2": test_utils.dummy_record_callable})


def test_invalid_output_tag(dummy_operation):
    with pytest.raises(exceptions.OperationIOError):
        dummy_operation.get_output("missing_tag")


def test_valid_output_tag(dummy_operation):
    with pytest.raises(NotImplementedError):
        dummy_operation.get_output("ot1")


def test_invalid_input_tag():
    with pytest.raises(exceptions.OperationIOError):
        DummyOperation({"it1": test_utils.dummy_record_callable,
                        "missing_tag": test_utils.dummy_record_callable})


def test_valid_input_tag(dummy_operation):
    pass