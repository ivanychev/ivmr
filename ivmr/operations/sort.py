import enum
import functools
from typing import Callable, Dict, Optional, Iterable, Tuple

from ivmr.operations import base
from ivmr.misc import exceptions
from ivmr.misc import ptypes
from ivmr.misc import utils

RecordComparator = Callable[[ptypes.Record, ptypes.Record], int]
RecordKeyFunc = Callable[[ptypes.Record], int]


class SortOrder(enum.Enum):
    ASC = 1
    DESC = 2


def _compare_records(rec1: ptypes.Record, rec2: ptypes.Record,
                     order: Tuple[Tuple[str, SortOrder]]) -> int:
    """Comparator of two records based on the given order."""
    for key, order_label in order:
        val1, val2 = rec1[key], rec2[key]
        if val1 < val2:
            return -1 if order_label == SortOrder.ASC else 1
        elif val1 > val2:
            return 1 if order_label == SortOrder.ASC else -1
    return 0


def _key_func_from_order(
        order: Iterable[Tuple[str, SortOrder]]) -> RecordKeyFunc:
    """Converts comparator to a key function, which's compatible with sorted."""

    comparator = lambda r1, r2: _compare_records(r1, r2, tuple(order))
    return functools.cmp_to_key(comparator)


class Sort(base.BaseOperation):
    INPUT_TAGS: Tuple[str, ...] = ("input_table",)
    OUTPUT_TAGS: Tuple[str, ...] = ("output_table",)

    def __init__(self,
                 order: Iterable[Tuple[str, SortOrder]],
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None,
                 cached: bool=False) -> None:
        """The sort operation for the compute graph.

        :param order: The sequence of keys and the orders of sorting.
        :param inputs: The mapping from input tags to record iterables.
        :param cached: bool. The parameter denotes if the operation should
            operate in cached mode.
        """
        base.BaseOperation.__init__(self, inputs, order, cached=cached)
        self._cached = cached
        self._cached_output = None
        self._key_func = _key_func_from_order(order)

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        """The implementation of the computation."""
        if tag != "output_table":
            raise exceptions.OperationIOError(
                "Sort received an invalid output tag")
        data_iterable = self._inputs["input_table"]()
        if self._cached and self._cached_output is not None:
            return self._cached_output
        values = sorted(list(data_iterable), key=self._key_func)

        if self._cached:
            self._cached_output = values
        return utils.iterable_to_generator(values)