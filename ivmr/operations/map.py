import itertools

from typing import Dict, Optional, Tuple

from ivmr.operations import base
from ivmr.misc import exceptions
from ivmr.misc import ptypes
from ivmr.misc import utils


class Map(base.BaseOperation):
    INPUT_TAGS: Tuple[str, ...] = ("input_table", )
    OUTPUT_TAGS: Tuple[str, ...] = ("output_table", )

    def __init__(self,
                 mapper: ptypes.GeneratorFactory,
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None,
                 cached: bool=False) -> None:
        """Map operation for the compute graph.

        :param mapper: The callable that returns a generator which processes
            each row.
        :param inputs: The mapping from input tags to record iterables.
        :param cached: bool. The parameter denotes if the operation should
            operate in cached mode.
        """
        base.BaseOperation.__init__(self, inputs, mapper, cached=cached)
        self._mapper = mapper
        self._cached = cached
        self._cached_output = None

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        """The implementation of the computation."""
        if tag != "output_table":
            raise exceptions.OperationIOError(
                "Mapper received an invalid output tag")
        generator_sequence = (self._mapper(record)
                              for record in self._inputs["input_table"]())
        record_sequence = itertools.chain.from_iterable(generator_sequence)
        if self ._cached:
            if self._cached_output is None:
                self._cached_output = list(record_sequence)
            return utils.iterable_to_generator(self._cached_output)
        return record_sequence
