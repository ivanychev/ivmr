import enum
import functools
import itertools
from typing import (
    Dict,
    Iterable,
    List,
    Optional,
    Tuple
)

import toolz

from ivmr.misc import ptypes
from ivmr.misc import utils
from ivmr.operations import base

PairOfRecords = Tuple[Optional[ptypes.Record], Optional[ptypes.Record]]


class JoinStrategy(enum.Enum):
    LEFT = 1
    RIGHT = 2
    INNER = 3
    OUTER = 4
    CROSS = 5


class Join(base.BaseOperation):
    INPUT_TAGS: Tuple[str, ...] = ("left_table", "right_table")
    OUTPUT_TAGS: Tuple[str, ...] = ("output_table",)

    def __init__(self,
                 leftkey: Optional[str],
                 rightkey: Optional[str],
                 strategy: JoinStrategy,
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None,
                 cached: bool=False):
        """Join operation that operates as the corresponding operation from SQL.

        :param leftkey: str. The key from left table which is to be used in
            joining.
        :param rightkey: str. The key from the right table wich is to be used in
            joining.
        :param strategy: JoinStrategy. The strategy of joining (LEFT, RIGHT,
            INNER, OUTER)
        :param inputs: The mapping from input tags to record iterables.
        :param cached: bool. The parameter denotes if the operation should
            operate in cached mode.
        """
        base.BaseOperation.__init__(self, inputs, leftkey, rightkey, strategy)
        self._leftkey = leftkey
        self._rightkey = rightkey
        self._strategy = strategy
        self._cached = cached
        self._cached_output = None

    def _merge_cross(
            self,
            left_records_seq: Iterable[ptypes.Record],
            right_records_seq: Iterable[ptypes.Record]
    ) -> ptypes.RecordGenerator:
        for lr, rr in itertools.product(left_records_seq, right_records_seq):
            yield toolz.dicttoolz.merge(lr, rr)

    def _merge(
            self,
            left_records_seq: Iterable[ptypes.Record],
            right_records_seq: Iterable[ptypes.Record],
            scheme: List[str]
    ) -> ptypes.RecordGenerator:
        """Merges two sequence of dicts in one sequence with the same keys.

        :param left_records_seq: An iterable of dicts.
        :param right_records_seq: An iterable of dicts.
        :param scheme: List of keys to be used in the merged iterable.
        :return: Record generator.
        """
        if self._strategy == JoinStrategy.CROSS:
            yield from self._merge_cross(left_records_seq, right_records_seq)
        merger = functools.partial(toolz.itertoolz.join,
                                   self._leftkey,
                                   left_records_seq,
                                   self._rightkey,
                                   right_records_seq)
        if self._strategy in (JoinStrategy.LEFT, JoinStrategy.OUTER):
            merger = functools.partial(merger, right_default=None)
        if self._strategy in (JoinStrategy.RIGHT, JoinStrategy.OUTER):
            merger = functools.partial(merger, left_default=None)

        # Here, merger() will yield pairs. We need to insert them in a single
        # record.
        for left_rec, right_rec in merger():
            # Both of them can vossibly be nones.
            left_rec = left_rec or {}
            right_rec = right_rec or {}
            rec = {key: None for key in scheme}
            rec.update(left_rec)
            rec.update(right_rec)
            yield rec

    def _extract_scheme(
            self,
            left_records_seq: ptypes.RecordGenerator,
            right_records_seq: ptypes.RecordGenerator):
        """Peeks first elements from each generator and deduces the schema of
            merged stream."""
        sentinel = dict()
        left_first_value, left_records_seq = utils.peek_from_generator(
            left_records_seq, sentinel)
        right_first_value, right_records_seq = utils.peek_from_generator(
            right_records_seq, sentinel)
        # Here left_first_value and right_first_value are first rows of the
        # tables or empty dicts. We're merging them so as to figure out what
        # keys will be present in the final table.
        scheme = toolz.dicttoolz.merge(left_first_value,
                                       right_first_value).keys()
        return scheme, left_records_seq, right_records_seq

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        """The implementation of the computation."""
        if self._cached and self._cached_output is not None:
            return utils.iterable_to_generator(self._cached_output)

        left_records_seq = self._inputs["left_table"]()
        right_records_seq = self._inputs["right_table"]()
        scheme, left_records_seq, right_records_seq = self._extract_scheme(
            left_records_seq, right_records_seq)
        merged_records = self._merge(left_records_seq,
                                     right_records_seq,
                                     scheme)
        if self._cached:
            self._cached_output = list(merged_records)
            return utils.iterable_to_generator(self._cached_output)
        return merged_records
