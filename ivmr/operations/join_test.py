import pytest

from ivmr.misc import utils
from ivmr.misc import test_utils
from ivmr.operations import join


@pytest.fixture
def left_data():
    raw_data = [
        ("John", 11),
        ("Sarah", 12),
        ("Ivan", 20),
        ("Sergey", 22)
    ]
    data = [{"name": x[0], "age": x[1]} for x in raw_data]
    return test_utils.to_operation_input(data)

@pytest.fixture
def right_data():
    raw_data = [
        ("Sarah", "Moscow"),
        ("Sarah", "New York"),
        ("Paul", "Kairo"),
        ("Sergey", "Paris")
    ]
    data = [{"person": x[0], "city": x[1]} for x in raw_data]
    return test_utils.to_operation_input(data)


@pytest.mark.parametrize("join_strategy, expected", [
    (
            join.JoinStrategy.INNER,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'}
            ],
    ),
    (
            join.JoinStrategy.LEFT,
            [
                {'name': 'John', 'age': 11, 'person': None, 'city': None},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Ivan', 'age': 20, 'person': None, 'city': None},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'}
            ],
    ),
    (
            join.JoinStrategy.RIGHT,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'},
                {'name': None, 'age': None, 'person': 'Paul', 'city': 'Kairo'}
            ],
    ),
    (
            join.JoinStrategy.OUTER,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'},
                {'name': None, 'age': None, 'person': 'Paul', 'city': 'Kairo'},
                {'name': 'John', 'age': 11, 'person': None, 'city': None},
                {'name': 'Ivan', 'age': 20, 'person': None, 'city': None}
            ],
    )
])
def test_join(left_data, right_data, join_strategy, expected):
    op = join.Join("name", "person", join_strategy,
                   inputs={"left_table": left_data, "right_table": right_data})
    result = op.get_output("output_table")
    actual = set(tuple(rec.items()) for rec in result)
    expected = set(tuple(rec.items()) for rec in expected)
    assert expected == actual


def test_cross_join():
    left_data = [
        {"1": 1, "2": 0},
        {"1": 0, "2": 1}
    ]
    right_data = [
        {"3": 1, "4": 1},
        {"3": 0, "4": 0}
    ]
    op = join.Join(None, None, join.JoinStrategy.CROSS,
                   inputs=
                   {"left_table": test_utils.to_operation_input(left_data),
                    "right_table": test_utils.to_operation_input(right_data)})
    result = op.get_output("output_table")
    actual = list(result)
    expected = [
        {"1": 1, "2": 0, "3": 1, "4": 1},
        {"1": 1, "2": 0, "3": 0, "4": 0},
        {"1": 0, "2": 1, "3": 1, "4": 1},
        {"1": 0, "2": 1, "3": 0, "4": 0}
    ]
    assert expected == actual


@pytest.mark.parametrize("join_strategy, expected", [
    (
            join.JoinStrategy.INNER,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'}
            ],
    ),
    (
            join.JoinStrategy.LEFT,
            [
                {'name': 'John', 'age': 11, 'person': None, 'city': None},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Ivan', 'age': 20, 'person': None, 'city': None},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'}
            ],
    ),
    (
            join.JoinStrategy.RIGHT,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'},
                {'name': None, 'age': None, 'person': 'Paul', 'city': 'Kairo'}
            ],
    ),
    (
            join.JoinStrategy.OUTER,
            [
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'Moscow'},
                {'name': 'Sarah', 'age': 12, 'person': 'Sarah', 'city': 'New York'},
                {'name': 'Sergey', 'age': 22, 'person': 'Sergey', 'city': 'Paris'},
                {'name': None, 'age': None, 'person': 'Paul', 'city': 'Kairo'},
                {'name': 'John', 'age': 11, 'person': None, 'city': None},
                {'name': 'Ivan', 'age': 20, 'person': None, 'city': None}
            ],
    )
])
def test_join_cached(left_data, right_data, join_strategy, expected):
    op = join.Join("name", "person", join_strategy,
                   inputs={"left_table": left_data, "right_table": right_data},
                   cached=True)
    result = op.get_output("output_table")
    actual = set(tuple(rec.items()) for rec in result)
    expected = set(tuple(rec.items()) for rec in expected)
    assert expected == actual

    result = op.get_output("output_table")
    actual = set(tuple(rec.items()) for rec in result)
    assert expected == actual
