import mock
import pytest

from ivmr.misc import ptypes
from ivmr.misc import utils
from ivmr.operations import map


@pytest.fixture
def fake_data():
    data = [{"first": x, "second": x} for x in range(5)]
    record_gen = (record for record in data)
    return {"input_table": utils.generator_to_callable(record_gen)}

def test_square_mapper(fake_data):
    def square(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] * rec["first"]}
        yield {"value": rec["second"] * rec["second"]}

    mapper = map.Map(square, inputs=fake_data)
    outputs = mapper.get_output("output_table")
    actual = list(outputs)

    expected = []
    for i in range(5):
        expected.extend([
            {"value": i ** 2},
            {"value": i ** 2}
        ])
    assert expected == actual


def test_sum_mapper(fake_data):
    def summer(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] + rec["second"]}

    mapper = map.Map(summer, inputs=fake_data)
    outputs = mapper.get_output("output_table")
    actual = list(outputs)

    expected = [{"value": i + i} for i in range(5)]
    assert expected == actual


def test_caching_disabled(fake_data):
    def summer(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] + rec["second"]}

    mapper = map.Map(summer, inputs=fake_data)
    outputs = mapper.get_output("output_table")
    actual = list(outputs)

    assert len(actual)

    outputs = mapper.get_output("output_table")
    actual = list(outputs)

    assert not len(actual)


def test_caching_enabled(fake_data):
    def summer(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] + rec["second"]}

    mapper = map.Map(summer, inputs=fake_data, cached=True)
    outputs = mapper.get_output("output_table")
    actual_one = list(outputs)

    assert len(actual_one)

    outputs = mapper.get_output("output_table")
    actual_two = list(outputs)

    assert len(actual_two)
    assert list(actual_one) == list(actual_two)
