import pytest

from ivmr.misc import utils
from ivmr.operations import sort

ASC = sort.SortOrder.ASC
DESC = sort.SortOrder.DESC


@pytest.fixture
def fake_data():
    raw_data = [
        (2, 3),
        (1, 1),
        (1, 3),
        (1, 2),
        (2, 1),
        (2, 2)
    ]
    data = [{"first": x[0], "second": x[1]} for x in raw_data]
    record_gen = (record for record in data)
    return {"input_table": utils.generator_to_callable(record_gen)}


@pytest.mark.parametrize("rec_one, rec_two, order, expected", [
    ({"f": 1, "s": 2}, {"f": 1, "s": 1}, (("f", ASC), ("s", ASC)), 1),
    ({"f": 1, "s": 2}, {"f": 1, "s": 1}, (("f", ASC),), 0),
    ({"f": 1, "s": 2}, {"f": 1, "s": 1}, (("f", DESC), ("s", ASC)), 1),
    ({"f": 1, "s": 2}, {"f": 1, "s": 1}, (("f", ASC), ("s", DESC)), -1),
    ({"f": 2, "s": 2}, {"f": 1, "s": 2}, (("f", DESC), ("s", ASC)), -1),
    ({"f": 2, "s": 2}, {"f": 1, "s": 200}, (("f", DESC), ("s", DESC)), -1)
])
def test_comparator(rec_one, rec_two, order, expected):
    assert expected == sort._compare_records(rec_one, rec_two, order)


@pytest.mark.parametrize("order, raw_expected", [
    ((("first", sort.SortOrder.ASC), ("second", sort.SortOrder.ASC)),
     [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3)]),
    ((("first", sort.SortOrder.DESC), ("second", sort.SortOrder.ASC)),
     [(2, 1), (2, 2), (2, 3), (1, 1), (1, 2), (1, 3)]),
    ((("second", sort.SortOrder.DESC), ("first", sort.SortOrder.ASC)),
     [(1, 3), (2, 3), (1, 2), (2, 2), (1, 1), (2, 1)]),
])
def test_sort(fake_data, order, raw_expected):
    sorter = sort.Sort(order, inputs=fake_data)
    actual = list(sorter.get_output("output_table"))
    expected = [{"first": x[0], "second": x[1]} for x in raw_expected]
    assert expected == actual


@pytest.mark.parametrize("order, raw_expected", [
    ((("first", sort.SortOrder.ASC), ("second", sort.SortOrder.ASC)),
     [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3)]),
    ((("first", sort.SortOrder.DESC), ("second", sort.SortOrder.ASC)),
     [(2, 1), (2, 2), (2, 3), (1, 1), (1, 2), (1, 3)]),
    ((("second", sort.SortOrder.DESC), ("first", sort.SortOrder.ASC)),
     [(1, 3), (2, 3), (1, 2), (2, 2), (1, 1), (2, 1)]),
])
def test_sort_cached(fake_data, order, raw_expected):
    sorter = sort.Sort(order, inputs=fake_data, cached=True)
    actual = list(sorter.get_output("output_table"))
    expected = [{"first": x[0], "second": x[1]} for x in raw_expected]
    assert expected == actual

    actual = list(sorter.get_output("output_table"))
    assert expected == actual