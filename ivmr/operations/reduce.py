import itertools
from typing import Dict, Iterable, List, Optional, Tuple

from ivmr.misc import exceptions
from ivmr.misc import ptypes
from ivmr.misc import utils
from ivmr.operations import base


def _is_not_sorted(records: List[ptypes.Record], keys: Iterable[str]):
    for i in range(len(records) - 1):
        current_rec = tuple(records[i][key] for key in keys)
        next_rec = tuple(records[i + 1][key] for key in keys)
        if current_rec > next_rec:
            print(f"current: {current_rec}, next: {next_rec}")
            return True
    return False


class Reduce(base.BaseOperation):
    INPUT_TAGS: Tuple[str, ...] = ("input_table",)
    OUTPUT_TAGS: Tuple[str, ...] = ("output_table",)

    def __init__(self,
                 reduce_key: ptypes.IterableOr[str],
                 reducer: ptypes.GeneratorFactory,
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None,
                 cached: bool = False):
        """Reduce operation used in the compute graph. The input table is
        grouped by a particular key and for each group new set of rows is
        yielded by the reducer.

        :param reduce_key: str. The key to group by.
        :param reducer: The callable that returns a generator which processes
            each group.
        :param inputs: The mapping from input tags to record iterables.
        :param cached: bool. The parameter denotes if the operation should
            operate in cached mode.
        """
        base.BaseOperation.__init__(self,
                                    inputs,
                                    reduce_key,
                                    reducer,
                                    cached=cached)
        self._cached = cached
        self._cached_output = None
        self._reduce_key = utils.to_iterable(reduce_key, str)
        self._reducer = reducer

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        """The implementation of the computation."""
        if tag != "output_table":
            raise exceptions.OperationIOError(
                "Reducer received an invalid output tag")

        if self._cached and self._cached_output is not None:
            return utils.iterable_to_generator(self._cached_output)

        records = list(self._inputs["input_table"]())
        if _is_not_sorted(records, self._reduce_key):
            raise exceptions.DataConditionError(
                f"Table is not sorted by f{self._reduce_key}")

        keyfunc = lambda r: tuple(r[key] for key in self._reduce_key)
        record_groups = (
            group
            for _, group in itertools.groupby(records, keyfunc)
        )
        generator_sequence = (
            self._reducer(group)
            for group in record_groups
        )
        record_sequence = itertools.chain.from_iterable(generator_sequence)

        if self._cached:
            self._cached_output = list(record_sequence)
            return utils.iterable_to_generator(self._cached_output)
        return record_sequence
