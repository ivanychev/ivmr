from typing import (
    Dict,
    Optional,
    Tuple
)

from ivmr.misc import ptypes
from ivmr.misc import exceptions


class BaseOperation(object):
    INPUT_TAGS: Tuple[str, ...] = tuple()
    OUTPUT_TAGS: Tuple[str, ...] = tuple()

    def __init__(self,
                 inputs: Optional[Dict[str, ptypes.RecIterableCallable]]=None,
                 *args, **kwargs) -> None:
        """Base class for all operation which can be used in the compute graph.

        :param inputs: The mapping from input tags to record iterables.
        :param args: additional arguments.
        :param kwargs: additional keyword arguments.
        """
        self._inputs: Optional[Dict[str, ptypes.RecIterableCallable]] = None
        self._verbose = False
        self._cached = False
        if inputs is not None:
            self.set_inputs(inputs)

    def get_output(self, tag: str) -> ptypes.RecordGenerator:
        """For a given output tag, it returns generator of records, which's the
        output of current operation.

        :param tag: str. The output tag, which must be in OUTPUT_TAGS.
        :return: Record generator.
        """
        if tag not in self.OUTPUT_TAGS:
            raise exceptions.OperationIOError(f"Output tag {tag} is invalid.")
        return self._get_output(tag)

    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        raise NotImplementedError()

    def set_inputs(self, inputs: Dict[str, ptypes.RecIterableCallable]) -> None:
        """Sets input for an input tag.

        :param inputs: the mapping from input tags to callables that return
            record generator.
        :returns: None.
        """
        if any(key not in self.INPUT_TAGS
               for key in inputs):
            raise exceptions.OperationIOError(
                f"Input tags are invalid for this operation."
                f"Expected {sorted(self.INPUT_TAGS)}, got {sorted(inputs)}.")
        if self._inputs:
            self._inputs.update(inputs)
        else:
            self._inputs = inputs

    def get_input_tags(self) -> Tuple[str, ...]:
        """Returns input tags of current operation."""
        return self.INPUT_TAGS

    def get_output_tags(self) -> Tuple[str, ...]:
        """Returns output tags of current operation."""
        return self.OUTPUT_TAGS

    def set_cached(self, cached: bool=False):
        """Sets operation to cached mode."""
        self._cached = cached

    def set_verbose(self, verbose: bool=True):
        """Sets operation to verbose mode."""
        self._verbose = verbose
