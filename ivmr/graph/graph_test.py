import pytest

from ivmr.graph import graph
from ivmr.misc import ptypes
from ivmr.operations import join
from ivmr.operations import map
from ivmr.operations import sort


@pytest.fixture
def fake_data():
    data = [{"first": x, "second": x} for x in range(5)]
    record_gen = (record for record in data)
    return record_gen


def test_simple_creation():
    g = graph.ComputeGraph("input", "output")
    g.add_operation(sort.Sort([("first", sort.SortOrder.ASC)]),
                    {"input_table": ("GRAPH", "input")},
                    "sort1")
    g.add_operation(join.Join("k", "l", join.JoinStrategy.INNER),
                    {"left_table": ("sort1", "output_table"),
                     "right_table": ("GRAPH", "input"),
                     "output_table": ("GRAPH", "output")},
                    "join2")
    assert True


def test_square_neg(fake_data):
    """Calculating square and multiplying by -1."""
    def square(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] * rec["first"]}

    def negative(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": -rec["value"]}

    g = graph.ComputeGraph("input", "output")
    g.add_operation(map.Map(square),
                    {"input_table": ("GRAPH", "input")},
                    op_id="square")
    g.add_operation(map.Map(negative),
                    {"input_table": ("square", "output_table"),
                     "output_table": ("GRAPH", "output")},
                    op_id="negative")
    g.set_input("input", fake_data)
    expected = [
        {'value': 0},
        {'value': -1},
        {'value': -4},
        {'value': -9},
        {'value': -16}
    ]
    assert expected == list(g.get_output("output"))


def test_multiple_outputs(fake_data):
    """Calculating square and multiplying by -1."""
    def square(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": rec["first"] * rec["first"]}

    def negative(rec: ptypes.Record) -> ptypes.RecordGenerator:
        yield {"value": -rec["first"]}

    g = graph.ComputeGraph("input", ["output1", "output2"])
    g.add_operation(map.Map(square),
                    {"input_table": ("GRAPH", "input"),
                     "output_table": ("GRAPH", "output1")},
                    op_id="square")
    g.add_operation(map.Map(negative),
                    {"input_table": ("GRAPH", "input"),
                     "output_table": ("GRAPH", "output2")},
                    op_id="negative")
    g.set_input("input", fake_data)
    expected1 = [
        {'value': 0},
        {'value': 1},
        {'value': 4},
        {'value': 9},
        {'value': 16}
    ]
    assert expected1 == list(g.get_output("output1"))

    expected2 = [
        {'value': 0},
        {'value': -1},
        {'value': -2},
        {'value': -3},
        {'value': -4}
    ]
    assert expected2 == list(g.get_output("output2"))
