import enum
import itertools

from typing import (
    Dict,
    Iterable,
    List,
    Optional,
    Tuple,
    TypeVar
)

from ivmr.misc import ptypes
from ivmr.misc import utils
from ivmr.misc import test_utils
from ivmr.operations import base

T = TypeVar('T')
OperationID = str
IoTag = str
Socket = Tuple[OperationID, IoTag]


class SocketType(enum.Enum):
    INPUT = 1
    OUTPUT = 2


def _sockets_to_iterable(sockets: ptypes.IterableOr[Socket]) -> Iterable[Socket]:
    """Makes sure that the passed object is an iterable of Sockets."""
    if (isinstance(sockets, Tuple) and len(sockets) == 2 and
            all(isinstance(field, str) for field in sockets)):
                return [sockets]
    return sockets


class CgStubOperation(base.BaseOperation):
    """Stub operation class for ComputeGraph."""
    def _get_output(self, tag: str) -> ptypes.RecordGenerator:
        return test_utils.empty_generator()


class ComputeGraph(object):
    GRAPH_KEY_WORD = "GRAPH"

    def __init__(self,
                 input_streams_id: Iterable[str],
                 output_streams_id: Iterable[str],
                 verbose: bool=False) -> None:
        """Compute graph initializer.

        :param input_streams_id: Iterable[str]. The input tags of the graph.
        :param output_streams_id: Iterable[str]. The output tags of the graph.

        :returns None.
        """
        input_streams_id = utils.to_iterable(input_streams_id, str)
        output_streams_id = utils.to_iterable(output_streams_id, str)

        self._graph_id = f"compute_graph_{str(id(self))}"
        self._graph_inputs: Dict[str, Optional[ptypes.RecordIterable]] = {
            id_: None for id_ in input_streams_id
        }
        self._graph_outputs: Dict[str,
                                  Optional[ptypes.RecGeneratorCallable]] = {
            id_: None for id_ in output_streams_id
        }
        self._edges: Dict[Socket, List[Socket]] = {
            (self.GRAPH_KEY_WORD, input_id): []
            for input_id in input_streams_id
        }
        self._operations: Dict[OperationID, Optional[base.BaseOperation]] = (
            dict([(self.GRAPH_KEY_WORD, CgStubOperation())]))
        self._verbose = verbose

    def _socket_type(self, socket: Socket) -> SocketType:
        """Checks whether the socket is input or output and returns the type."""
        op_id, tag = socket

        # Special case when the socket belongs to graph.
        if op_id == self.GRAPH_KEY_WORD:
            if tag in self._graph_inputs:
                return SocketType.OUTPUT
            if tag in self._graph_outputs:
                return SocketType.INPUT
            raise ValueError(f"Graph doesn't have IO tag {tag}.")

        if op_id not in self._operations:
            raise ValueError(f"There's no operation with id {op_id}")
        op = self._operations[socket[0]]
        input_tags, output_tags = op.get_input_tags(), op.get_output_tags()
        if tag in input_tags:
            return SocketType.INPUT
        elif tag in output_tags:
            return SocketType.OUTPUT
        raise ValueError(f"Invalid IO tag {tag} for operation {op_id}")

    def _add_edge(self, first_socket: Socket, second_socket: Socket):
        """Adds the edge which's a pair of sockets to the graph."""
        first_type = self._socket_type(first_socket)
        second_type = self._socket_type(second_socket)
        if first_type == second_type:
            raise ValueError(
                f"Both sockets {{{first_socket}, {second_socket}}} "
                f"are both of {first_type} type"
            )

        if first_type is SocketType.INPUT:
            input_socket, output_socket = first_socket, second_socket
        else:
            input_socket, output_socket = second_socket, first_socket

        self._edges.setdefault(output_socket, []).append(input_socket)

        # Updating data callable.
        input_op_id, input_tag = input_socket
        if input_op_id == self.GRAPH_KEY_WORD:
            self._graph_outputs[input_tag] = self._stream_callable(output_socket)
        else:
            op = self._operations[input_op_id]
            op.set_inputs({input_tag: self._stream_callable(output_socket)})

    def _update_edges(
            self,
            operation_id: OperationID,
            io_tag_to_socket: Dict[IoTag, ptypes.IterableOr[Socket]]) -> None:
        """Connects sockets of current operation to other nodes.

        :param operation_id: str. The id of operation which is to be connected.
        :param io_tag_to_socket: Dict[IoTag, IterableOr[Socket]]. The mapping
            from the op's socket to the endpoint(s). An endpoint is a socket
            of some other node or the graph itself.
        :returns: None.
        """
        for tag, sockets in io_tag_to_socket.items():
            # So as to make it iterable.
            sockets = _sockets_to_iterable(sockets)
            our_socket = (operation_id, tag)
            for away_socket in sockets:
                self._add_edge(our_socket, away_socket)

    def add_operation(self,
                      op: base.BaseOperation,
                      io_tag_to_socket: Dict[IoTag, ptypes.IterableOr[Socket]],
                      op_id: Optional[str]=None) -> None:
        """Adds new operation to the graph.

        :param op: BaseOperation. The operation to add.
        :param io_tag_to_socket: Dict[IoTag, IterableOr[Socket]]. The mapping
            from the op's socket to the endpoint(s). An endpoint is a socket
            of some other node or the graph itself.
        :param op_id: Optional[str]. The id string of the operation being added.
            If not passed, it will be generated automatically.
        :returns: None.
        """
        # Checking arguments.
        valid_tags = set(itertools.chain(op.get_input_tags(),
                                         op.get_output_tags()))
        for tag in io_tag_to_socket:
            if tag not in valid_tags:
                raise ValueError(f"IO tag is not valid for current operation")

        # Checking operation id.
        op_id = op_id or f"operation_{id(op)}"
        if op_id in self._operations:
            raise ValueError(f"Operation {op_id} is already present in the "
                             f"graph")

        self._operations[op_id] = op
        op.set_cached(cached=True)
        op.set_verbose(verbose=self._verbose)
        self._update_edges(op_id, io_tag_to_socket)

    def set_input(self,
                  input_stream_id: str,
                  stream: ptypes.RecordIterable) -> None:
        """Provide graph with input.

        :param input_stream_id: Iterable[dict]. The iterable of dictionaries.
        :param stream: str. The input tag to correspond the data to.
        :returns: None.
        """
        if input_stream_id not in self._graph_inputs:
            raise ValueError(f"Input tag {input_stream_id} is missing")
        self._graph_inputs[input_stream_id] = list(stream)

    def get_output(self, output_stream_id: str) -> ptypes.RecordIterable:
        """Returns an iterable of records that correspond to the output tag."""
        return self._graph_outputs[output_stream_id]()

    def _stream_callable(self, socket: Socket) -> ptypes.RecIterableCallable:
        """Constructs a callable which returns the output sequence.

        :param socket: Tuple[str, str]. The source socket.
        :returns: A callable with no parameters that returns an iterable of
            records.
        """
        op_id, tag = socket
        if op_id == self.GRAPH_KEY_WORD:
            return lambda: self._graph_inputs[tag]
        return lambda: self._operations[op_id].get_output(tag)

    def __repr__(self) -> str:
        buffer = [
            "Graph",
            str(self._operations),
            str(self._edges)
        ]
        return "\n".join(buffer)
